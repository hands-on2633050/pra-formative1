import java.util.*;
public class Formative5 {
 public static void main(String args[]){
   HashMap<Integer,String> map=new HashMap<Integer,String>(); // Initialize HashMap
   map.put(1,"Mango");
   map.put(2,"Apple");
   map.put(3,"Banana");
   map.put(4,"Grapes");
   map.put("pisang","Pisang"); // Ada error, tidak sesuai deklarasi tipe (Integer, String)

   System.out.println("Iterating Hashmap...");
   for(Map.Entry m : map.entrySet()){ // foreach semua item dari metode entrySet hashmap (return Set berisi key-value pair hashmap)
    System.out.println(m.getKey()+" "+m.getValue()); // print key dan value dari set tersebut
   }
   
  }
}

/*
 * Kode ini tidak bisa jalan karena ada error, "pisang" di letakkan sebagai key untuk hashnap yang diberikan tipe <Integer, String>
 * 
 * Ini bisa dikoreksi dengan menukkar "pisang" menjadi 5
 */


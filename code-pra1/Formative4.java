import java.util.Scanner;

public class Formative4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Masukkan Angka Pertama: ");
        int angkaPertama = scanner.nextInt();

        System.out.print("Masukkan Angka Kedua: ");
        int angkaKedua = scanner.nextInt();

        System.out.printf("Hasil penjumlahan dari %d dan %d adalah: %d", angkaPertama, angkaKedua, sum(angkaPertama, angkaKedua));

        scanner.close();
    }

    static int sum(int angkaPertama, int angkaKedua){

        return angkaPertama + angkaKedua;
    }
}

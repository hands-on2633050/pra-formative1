public class Task1 {
  public static void main(String[] args) {
    int i = 1; // initialization

    // do-while loop. Memastikan setidaknya code block dijalankan sekali
    do {
      if (i == 5) {
        // Jika i adalah 5, increment lalu keluar dari loop (break) walaupun tidak masih memenuhi kondisi while-loop
        i++;
        break;
      }
      System.out.println(i); // print value i
      i++; // increment i
    } while (i <= 10); // selama i kurang atau sama dengan 10 jalankan loop lagi
  }
}

/*
 *  Hasil dari code ini adalah:
 * 1
 * 2
 * 3
 * 4
 * 
 * Kondisi while loop masih terpenuhi di saat program berhenti karena sebelum i lebih dari 10 akan
 * bertemu dengan kondisi yang akan melakukan break;
 * 
 */

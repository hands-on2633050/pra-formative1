import java.util.*;
public class Task4 {
  public static void main(String args[]) {
    ArrayList<String> list = new ArrayList<String>(); // initialization ArrayList

    // Tambahkan element list
    list.add("Mango");
    list.add("Apple");
    list.add("Banana");
    list.add("Grapes");

    // print list
    // java menggunakan toString() yang dimiliki oleh ArrayList, sehingga menampilkan represntasi list dalam bentuk string
    System.out.println(list);
  }
}

/*
 * Hasilnya adalah semua isi ArrayList yang ada di print
 */


/*
 * PERBEDAAN dengan Task3
 * Task3 print element ArrayList satu per satu dengan mengubahnya menjadi iterator dan menggunakan loop
 * 
 * 
 */



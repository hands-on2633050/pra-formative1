public class Task2 {
  public static void main(String[] args) {
    int age = 25; // initialization
    int weight = 48; // initialization

    // Nested if else statements

    // pasangan if-else pertama, jika age 18 jalankan pasangan if-else kedua, jika tidak print "Age must be greater than 18"
    if (age >= 18) {

      // pasangan if-else kedua, jika weight lebih dari 50 bisa donasi dara, jika tidak tidak bisa donasi darah
      if (weight > 50) {
        System.out.println("You are eligible to donate blood");
      } else {
        System.out.println("You are not eligible to donate blood");
      }
    } else {
      System.out.println("Age must be greater than 18");
    }
  }
}


/*
 * 
 * Walaupun umur lebih dari 25, berat dibawah 50 jadi tidak bisa donasi darah
 */
import java.util.*;
public class Task3{
  public static void main(String args[]) {
    ArrayList<String> list = new ArrayList<String>(); // initialization ArrayList

    // Tambahkan element list
    list.add("Mango");
    list.add("Apple");
    list.add("Banana");
    list.add("Grapes");
    Iterator<String> itr = list.iterator(); // itr debirikan value Iterator yang dibuat dari ArrayList

    while (itr.hasNext()) {// while loop yang berjalan selama iterator yang dibuat masih ada elemen selanjutnya
      System.out.println(itr.next()); 
      // print element iterator selanjutnya dan pindahkan current element (jika tidak ada, hasNext() false)
    }
  }
}

/*
 * Hasilnya adalah semua isi ArrayList yang ada di print
 */

 /*
 * PERBEDAAN dengan Task4
 * Task4 print list langsung, sehingga java menggunakan metode toString yang dimiliknya untuk
 * menampilkan representasi String list tersebut
 * 
 * 
 */
